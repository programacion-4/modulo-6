import requests
import numpy as np
import os
def obtener(url):
    next = url
    data = []
    while next:
        res = requests.get(next)
        res = res.json()
        next = res['next']    
        for item in res['results']:
            data.append(item)
    return data


def getPlanetsUrlByWheather(url,clima):
    planets = obtener(url)
    data = []
    for item in planets:
        if clima in item['climate']:
            data.append(item['url'])
    return data

def peliculas_con_planetas_aridos(url,planets):
    planets = np.asarray(planets)
    peliculas = obtener(url)
    data = []
    for item in peliculas:   
        planet = np.asarray(item['planets'])
        if np.any(np.in1d(planets, planet)):
            data.append(item)
    return data



def naveMasGrande(url):
    waves = obtener(url)
    data = waves[0]
    for item in waves:
        if float(item['length'].replace(',', '')) > float(data['length'].replace(',', '')):
            data = item
    return data



def especieEnPelicula(pelicula,especie):
    req = requests.get(pelicula)
    req = req.json()
    peliculaPersonajes = req['characters']
    res = []
    for item in peliculaPersonajes:
        reqNameSpecie = requests.get(item).json()
        nameSpecie = reqNameSpecie['species']
        try:
            if especie in nameSpecie:
                res.append(item)
        except:
            pass
        
    return res

if __name__ == "__main__":
    print('\nBIENCENIDO A SWAPI\n')
    print('Películas en la que aparecen planetas cuyo clima sea árido\n')
    peliculas_plAridos = peliculas_con_planetas_aridos('https://swapi.dev/api/films/',getPlanetsUrlByWheather('https://swapi.dev/api/planets/','arid'))
    for item in peliculas_plAridos:
        print(f'Episodio:{item["episode_id"]} - Titulo:{item["title"]}\n')
    print(f'totla:{len(peliculas_plAridos)}\n')
    print('Wookies que aparecen en la sexta película\n')
    especie_aparicion = especieEnPelicula('http://swapi.dev/api/films/6/','http://swapi.dev/api/species/3/')
    for item in especie_aparicion:
        res = requests.get(item).json()
        print(f'Name:{res["name"]} - Genero:{res["gender"]} - url:{res["url"]}\n')
    print(f'total:{len(especie_aparicion)}\n')
    print('La nave mas grande\n')
    print(naveMasGrande('https://swapi.dev/api/starships/'))
    os.system("pause")
